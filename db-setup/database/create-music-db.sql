CREATE DATABASE music;

CREATE TABLE author (
    id serial8 NOT NULL PRIMARY KEY,
    "name" varchar(150) NULL,
    link varchar(100) NOT NULL,
    rebounds_count int4 NULL,
    views_count int4 NULL,
    date_load timestamp NULL DEFAULT now()
);


CREATE TABLE songs (
    id serial8 NOT NULL PRIMARY KEY,
    author_id int8 NOT NULL REFERENCES author(id),
    "name" varchar(150) NULL,
    link varchar(200) NOT NULL,
    views_count int4 NULL,
    "text" json NULL,
    date_load timestamp NULL DEFAULT now()
);
